package com.wiktor.crosssection_v10.pages.mainActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.pages.mainActivity.adapters.MyPagerAdapter;

public class MainActivity extends AppCompatActivity {

    FragmentPagerAdapter pagerAdapter;

    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.view_pager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.getBackStackEntryCount();

        pagerAdapter = new MyPagerAdapter(fragmentManager);

        pagerAdapter.notifyDataSetChanged();

        tabLayout = findViewById(R.id.tab_layout);
        viewPager.setAdapter(pagerAdapter);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.exit:
                finish();
                break;
            case R.id.info:
                createInfoDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createInfoDialog() {
        final AlertDialog.Builder dialogInfo = new AlertDialog.Builder(this);
        dialogInfo.setTitle("Инфо")
                .setView(R.layout.dialog_with_image)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = dialogInfo.create();
        alert.show();
    }

}
