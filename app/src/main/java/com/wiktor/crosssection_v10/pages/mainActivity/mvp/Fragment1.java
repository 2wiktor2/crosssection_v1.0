package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wiktor.crosssection_v10.Constants;
import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.pages.saveHistory.SaveFormActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


// В этом фрагменте вычисляется сечение многожильного провода

public class Fragment1 extends Fragment implements View.OnClickListener, Fragment1Contract.View {

    @BindView(R.id.edit_text_fragment_1)
    EditText inputDiameter;
    @BindView(R.id.edit_text2_fragment_1)
    EditText inputCount;
    @BindView(R.id.text_view3_fragment1)
    TextView textViewResult;
    @BindView(R.id.fab_fragment1)
    FloatingActionButton fab1;

    private Double result;
    private double diameterFragment1;
    private int countZH;

    Fragment1Contract.Presenter fragment1Presenter = new Fragment1Presenter(this);


    public static Fragment1 newInstance() {
        Fragment1 fragment1 = new Fragment1();
        return fragment1;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        fab1.setOnClickListener(this);

        if (result != null) {
            textViewResult.setText(Double.toString(result));
        }
        inputDiameter.addTextChangedListener(textWatcher);
        inputCount.addTextChangedListener(textWatcher);
    }

    @Override
    public void showResult(Double res) {
        result = res;
        textViewResult.setText(Double.toString(result));
    }

    public void getNumbers() {
        //todo отловить  java.lang.NumberFormatException: For input string: "."
        if (!inputDiameter.getText().toString().equals("")) {
            //проверка, был ли введен 0 перед точкой. чтобы не выскочил NumberFormatException
            if (inputDiameter.getText().toString().startsWith(".")) {
                String tempStr = "0" + inputDiameter.getText().toString();
                diameterFragment1 = Double.parseDouble(tempStr);
            } else {
                diameterFragment1 = Double.parseDouble(inputDiameter.getText().toString());
            }
        }
        if (!inputCount.getText().toString().equals(""))
            countZH = Integer.parseInt(inputCount.getText().toString());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getNumbers();
            fragment1Presenter.start(diameterFragment1, countZH);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_fragment1:
                if (result != null) {
                    Intent intent = new Intent(getActivity(), SaveFormActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_FIRST, inputDiameter.getText().toString());
                    bundle.putString(Constants.KEY_SECOND, inputCount.getText().toString());
                    bundle.putString(Constants.KEY_RESULT, result.toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }
}
