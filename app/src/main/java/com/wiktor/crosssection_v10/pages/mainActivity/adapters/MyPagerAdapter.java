package com.wiktor.crosssection_v10.pages.mainActivity.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wiktor.crosssection_v10.Constants;
import com.wiktor.crosssection_v10.pages.mainActivity.mvp.Fragment1;
import com.wiktor.crosssection_v10.pages.mainActivity.mvp.Fragment2;
import com.wiktor.crosssection_v10.pages.mainActivity.mvp.Fragment3;
import com.wiktor.crosssection_v10.pages.mainActivity.fragments.HistoryFragment;

public class MyPagerAdapter extends FragmentPagerAdapter {

    private String[] tabsForTabLayout;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
        tabsForTabLayout = new String[]{"1", " 2", "3", "История"};
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return Fragment1.newInstance();
            case 1:
                return Fragment2.newInstance();
            case 2:
                return Fragment3.newInstance();
            case 3:
                return HistoryFragment.newInstance();
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabsForTabLayout[position];
    }

    @Override
    public int getCount() {
        return Constants.ITEMS_COUNT_FOR_PAGER_ADAPTER;
    }


}
