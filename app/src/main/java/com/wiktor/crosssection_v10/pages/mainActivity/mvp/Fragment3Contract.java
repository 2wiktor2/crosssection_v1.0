package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

public interface Fragment3Contract {
    interface View {
        void showResult(Double res);
        void showToast(Double diam);
    }

    interface Fragment3Presenter {
        void start(Double length, Double count);
    }
}
