package com.wiktor.crosssection_v10.pages.mainActivity.fragments;

public interface HistoryContract {
    interface View{
        void showSavedDataModel();
    }
    interface Presenter{
        void start();
    }
    interface Model{
         void getData();
    }
}
