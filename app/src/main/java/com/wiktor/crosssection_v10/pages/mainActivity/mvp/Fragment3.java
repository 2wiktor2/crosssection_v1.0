package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wiktor.crosssection_v10.Constants;
import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.pages.saveHistory.SaveFormActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


// В этом фрагменте вычисляется сечение одножильного  провода без замера диаметра

public class Fragment3 extends Fragment implements View.OnClickListener, Fragment3Contract.View {

    @BindView(R.id.edit_text_fragment_3)
    EditText editTextLength;
    @BindView(R.id.edit_text2_fragment_3)
    EditText editTextTurns;
    @BindView(R.id.text_view3_fragment3)
    TextView textViewResult;
    @BindView(R.id.fab_fragment3)
    FloatingActionButton fab3;
    @BindView(R.id.text_view_diameter2_fragment3)
    TextView textViewDiameter;
    private double length;
    private double turns;

    private Double result;
    private Double diameter ;

    Fragment3Contract.Fragment3Presenter fragment3Presenter= new Fragment3Presenter(this);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment3, container, false);
        return view;
    }

    public static Fragment3 newInstance() {
        Fragment3 fragment3 = new Fragment3();
        return fragment3;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        fab3.setOnClickListener(this);

        if (result != null) {
            textViewResult.setText(Double.toString(result));
        }
        editTextLength.addTextChangedListener(textWatcher);
        editTextTurns.addTextChangedListener(textWatcher);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_fragment3:
                if (result != null) {
                    Intent intent = new Intent(getActivity(), SaveFormActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_FIRST, diameter.toString());
                    bundle.putString(Constants.KEY_SECOND, "1");
                    bundle.putString(Constants.KEY_RESULT, result.toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getNumbers();
            fragment3Presenter.start(length, turns);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void showResult(Double res) {
        result = res;
        textViewResult.setText(Double.toString(res));
    }

    @Override
    public void showToast(Double diam) {
        diameter = diam;
        textViewDiameter.setText(""+diam);
       // Toast.makeText(getActivity(), "Диаметр = " + diam, Toast.LENGTH_SHORT).show();
    }


    public void getNumbers() {
        if (!editTextLength.getText().toString().equals("")) {
            //проверка, был ли введен 0 перед точкой. чтобы не выскочил NumberFormatException
            if (editTextLength.getText().toString().startsWith(".")) {
                String tempStr = "0" + editTextLength.getText().toString();
                length = Double.parseDouble(tempStr);
            } else {
                length = Double.parseDouble(editTextLength.getText().toString());
            }
        }
        if (!editTextTurns.getText().toString().equals(""))
            turns = Double.parseDouble(editTextTurns.getText().toString());
    }

}
