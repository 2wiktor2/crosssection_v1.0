package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

public interface Fragment1Contract {
    interface View {
    void showResult(Double res);
    }
    interface Presenter {
        void start(Double diam, int count);
    }
}
