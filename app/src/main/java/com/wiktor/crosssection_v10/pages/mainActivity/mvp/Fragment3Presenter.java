package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Fragment3Presenter implements Fragment3Contract.Fragment3Presenter{
    Fragment3Contract.View viewFragment3;

    public Fragment3Presenter(Fragment3Contract.View viewFragment3) {
        this.viewFragment3 = viewFragment3;
    }

    // Расчет сечения для одножильного провода 3-тий способ
    public void calculateThirdFragment3(Double length, Double count) {

    }


    // Расчет сечения для одножильного провода 3-тий способ
    @Override
    public void start(Double length, Double count) {
        Double diam = length / count;
        if (Double.isNaN(diam) || Double.isInfinite(diam)) {
            diam = 0.0;
        }
        BigDecimal bigDecimal1 = new BigDecimal(diam).setScale(3, RoundingMode.HALF_EVEN);
        diam = bigDecimal1.doubleValue();

        Double res = diam * diam * 0.785;
        // округлить до 3-го числа после запятой
        BigDecimal bigDecimal = new BigDecimal(res).setScale(3, RoundingMode.HALF_EVEN);
        res = bigDecimal.doubleValue();

        viewFragment3.showResult(res);
        viewFragment3.showToast(diam);

    }
}
