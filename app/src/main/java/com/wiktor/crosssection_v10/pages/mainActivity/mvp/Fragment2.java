package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wiktor.crosssection_v10.Constants;
import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.pages.saveHistory.SaveFormActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


// В этом фрагменте вычисляется сечение одножильного провода

public class Fragment2 extends Fragment implements View.OnClickListener, Fragment2Contract.View {

    @BindView(R.id.edit_text_fragment_2)
    EditText inputDiameter;
    @BindView(R.id.text_view3_fragment2)
    TextView textViewResult;
    @BindView(R.id.fab_fragment2)
    FloatingActionButton fab2;

    Fragment2Contract.Presenter fragment2Presenter = new Fragment2Presenter(this);

    private Double result;
    private double diameterFragment2 = 0;

    public static Fragment2 newInstance() {
        Fragment2 fragment2 = new Fragment2();
        return fragment2;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        fab2.setOnClickListener(this);

        if (result != null) {
            textViewResult.setText(Double.toString(result));
        }
        inputDiameter.addTextChangedListener(textWatcher);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_fragment2:
                if (result != null) {
                    Intent intent = new Intent(getActivity(), SaveFormActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_FIRST, inputDiameter.getText().toString());
                    bundle.putString(Constants.KEY_SECOND, "1");
                    bundle.putString(Constants.KEY_RESULT, result.toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getNumbers();
            fragment2Presenter.start(diameterFragment2);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void showResult(Double res) {
        result = res;
        textViewResult.setText(Double.toString(result));
    }

    public void getNumbers() {
        if (!inputDiameter.getText().toString().equals(""))
            //проверка, был ли введен 0 перед точкой. чтобы не выскочил NumberFormatException
            if (inputDiameter.getText().toString().startsWith(".")) {
                String tempStr = "0" + inputDiameter.getText().toString();
                diameterFragment2 = Double.parseDouble(tempStr);
            } else {
                diameterFragment2 = Double.parseDouble(inputDiameter.getText().toString());
            }
    }
}
