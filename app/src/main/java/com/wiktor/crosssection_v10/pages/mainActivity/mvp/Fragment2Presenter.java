package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Fragment2Presenter implements Fragment2Contract.Presenter {
    Fragment2Contract.View viewFragment2;

    public Fragment2Presenter(Fragment2Contract.View viewFragment2) {
        this.viewFragment2 = viewFragment2;
    }

    // Расчет сечения для одножильного провода
    @Override
    public void start(Double diam) {
        Double res = diam * diam * 0.785;
        // округлить до 3-го числа после запятой
        BigDecimal bd = new BigDecimal(res).setScale(3, RoundingMode.HALF_EVEN);
        res = bd.doubleValue();

        viewFragment2.showResult(res);
    }
}
