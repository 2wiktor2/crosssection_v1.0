package com.wiktor.crosssection_v10.pages.mainActivity.mvp;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Fragment1Presenter implements Fragment1Contract.Presenter {
    Fragment1Contract.View viewFragment1;

    public Fragment1Presenter(Fragment1Contract.View viewFragment1) {
        this.viewFragment1 = viewFragment1;
    }

    // Расчет сечения для многожильного провода
    @Override
    public void start(Double diam, int count) {
        Double res = diam * diam * 0.785 * count;
        // округлить до 3-го числа после запятой
        BigDecimal bd = new BigDecimal(res).setScale(3, RoundingMode.HALF_EVEN);
        res = bd.doubleValue();

        viewFragment1.showResult(res);
    }
}
