package com.wiktor.crosssection_v10.pages.mainActivity.fragments;

public class HistoryPresenter implements HistoryContract.Presenter{

    HistoryContract.View view;

    HistoryContract.Model model = new HistoryModel();

    public HistoryPresenter(HistoryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        view.showSavedDataModel();
    }
}
