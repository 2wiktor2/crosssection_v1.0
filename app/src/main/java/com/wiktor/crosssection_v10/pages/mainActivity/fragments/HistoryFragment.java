package com.wiktor.crosssection_v10.pages.mainActivity.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.dataBase.DataModel;
import com.wiktor.crosssection_v10.dataBase.DbHelper;
import com.wiktor.crosssection_v10.pages.mainActivity.adapters.RVAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends Fragment implements View.OnClickListener, HistoryContract.View {

    HistoryContract.Presenter presenter = new HistoryPresenter(this);

    @BindView(R.id.recycler_container) RecyclerView recyclerView;
    @BindView( R.id.fab_history_fragment) FloatingActionButton floatingActionButton;

    ArrayList <DataModel> dataModels = new ArrayList <>();
    DbHelper dbHelper;
    RVAdapter rvAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_fragment, container, false);
        return view;
    }

    public static HistoryFragment newInstance() {
        HistoryFragment historyFragment = new HistoryFragment();
        return historyFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        floatingActionButton.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        presenter.start();

        dbHelper = new DbHelper(getContext());

    }

    @Override
    public void onResume() {
        super.onResume();
        dataModels = dbHelper.getData(dataModels);
        rvAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        onCreateAlertDialog();
    }

    public void onCreateAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Удаление истории")
                .setMessage("Удалить все сохраненные данные?")
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DbHelper dbHelper = new DbHelper(getActivity());
                        dbHelper.getWritableDatabase().delete(DbHelper.NAME_OF_TABLE, null, null);
                        Toast.makeText(getActivity(), "История отчищена", Toast.LENGTH_LONG).show();
                        rvAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void showSavedDataModel() {
        rvAdapter = new RVAdapter(dataModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(rvAdapter);
    }
}

