package com.wiktor.crosssection_v10.pages.saveHistory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wiktor.crosssection_v10.Constants;
import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.dataBase.DbHelper;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaveFormActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_data)
    TextView textViewDataTime;
    @BindView(R.id.tv_sechenie)
    TextView textViewResult;
    @BindView(R.id.tv_diam)
    TextView textViewDiam;
    @BindView(R.id.tv_count)
    TextView textViewCount;
    @BindView(R.id.edit_text_save_form)
    EditText editTextNotes;

    @BindView(R.id.button_save_save_form_Activity)
    Button buttonSave;

    private String dataTime;
    private String result;
    private String diam;
    private String count;
    private String notes;

    private DbHelper dbHelper = new DbHelper(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_form);
        ButterKnife.bind(this);

        buttonSave.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            diam = bundle.getString(Constants.KEY_FIRST);
            count = bundle.getString(Constants.KEY_SECOND);
            result = bundle.getString(Constants.KEY_RESULT);
        }
        dataTime = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

        textViewDataTime.setText("Дата: " + dataTime);
        textViewResult.setText("Сечение: " + result);
        textViewDiam.setText("Диаметр: " + diam);
        textViewCount.setText("Колличество жил: " + count);
    }

    @Override
    public void onClick(View v) {
        notes = editTextNotes.getText().toString();
        Log.d("MyLog",
                " data= " + dataTime +
                        " result= " + result +
                        " diam= " + diam +
                        " count= " + count +
                        " notes= " + notes);
        addRow();
        closeActivity();
    }

    private void addRow() {
        notes = editTextNotes.getText().toString();
        dbHelper.createRow(dataTime, result, diam, count, notes);
        Toast.makeText(this, "Запись сохранена", Toast.LENGTH_SHORT).show();

    }

    private void closeActivity() {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dbHelper.close();
    }
}
