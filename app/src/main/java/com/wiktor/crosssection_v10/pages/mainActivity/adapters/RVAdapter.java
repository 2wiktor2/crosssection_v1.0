package com.wiktor.crosssection_v10.pages.mainActivity.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiktor.crosssection_v10.R;
import com.wiktor.crosssection_v10.dataBase.DataModel;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RVAdapter extends RecyclerView.Adapter <RVAdapter.ViewHolder> {

    private ArrayList <DataModel> dataModels;

    public RVAdapter(ArrayList <DataModel> dataModels) {
        this.dataModels = dataModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(dataModels.get(i));
    }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_data_in_list_item)
        TextView dataTame;
        @BindView(R.id.tv_sechenie_in_list_item)
        TextView result;
        @BindView(R.id.tv_diam_in_list_item)
        TextView diameter;
        @BindView(R.id.tv_count_in_list_item)
        TextView count;
        @BindView(R.id.notes_in_list_item)
        TextView notes;
        @BindString(R.string.item_text_sechenie)
        String stringSechenie;
        @BindString(R.string.item_text_diameter)
        String stringDiameter;
        @BindString(R.string.item_text_count)
        String stringCount;
        @BindString(R.string.mm_u00b2)
        String stringMm2;
        @BindString(R.string.mm)
        String stringMm;
        @BindString(R.string.sht)
        String stringSht;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(DataModel dataModel) {
            dataTame.setText(dataModel.getDataTime());
            result.setText(stringSechenie + dataModel.getResult() + stringMm2);
            diameter.setText(stringDiameter + dataModel.getDiameter() + stringMm);
            count.setText(stringCount + dataModel.getCount() + stringSht);
            notes.setText(dataModel.getNotes());
        }

    }
}
