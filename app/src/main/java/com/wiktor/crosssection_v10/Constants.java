package com.wiktor.crosssection_v10;

public class Constants {
    public static final int ITEMS_COUNT_FOR_PAGER_ADAPTER = 4;

    public static final String KEY_FIRST = "key_for_first_number";
    public static final String KEY_SECOND = "key_for_second_number";
    public static final String KEY_RESULT = "key_for_result";

}
