package com.wiktor.crosssection_v10.dataBase;

public class DataModel {

    private String dataTime;
    private String result;
    private String diameter;
    private String count;
    private String notes;

    public DataModel(String dataTime, String result, String diameter, String count, String notes) {
        this.dataTime = dataTime;
        this.result = result;
        this.diameter = diameter;
        this.count = count;
        this.notes = notes;
    }

    public String getDataTime() {
        return dataTime;
    }

    public String getResult() {
        return result;
    }

    public String getDiameter() {
        return diameter;
    }

    public String getCount() {
        return count;
    }

    public String getNotes() {
        return notes;
    }
}