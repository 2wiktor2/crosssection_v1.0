package com.wiktor.crosssection_v10.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DbHelper extends SQLiteOpenHelper {

    public static final String NAME_OF_TABLE = "SavedHistory";

    final static int VERSION = 1;

    final String COLUMN_ID = "ColumnId";
    final String COLUMN_DADA_TIME = "ColumnDataTime";
    final String COLUMN_RESULT = "ColumnSechenie";
    final String COLUMN_DIAMETER = "ColumnDiameter";
    final String COLUMN_COUNT = "ColumnCount";
    final String COLUMN_NOTES = "ColumnNotes";

    public DbHelper(Context context) {
        super(context, "DBForCrossSection", null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" Create table " + NAME_OF_TABLE + " ( " +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_DADA_TIME + " text, " +
                COLUMN_RESULT + " text, " +
                COLUMN_DIAMETER + " text," +
                COLUMN_COUNT + " text," +
                COLUMN_NOTES + " text); ");
    }


    public void createRow(String data, String result, String diameter, String count, String notes) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_DADA_TIME, data);
        cv.put(COLUMN_RESULT, result);
        cv.put(COLUMN_DIAMETER, diameter);
        cv.put(COLUMN_COUNT, count);
        cv.put(COLUMN_NOTES, notes);

        long rowId = db.insert(NAME_OF_TABLE, null, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList <DataModel> getData(ArrayList <DataModel> arrayList) {
        arrayList.clear();

        SQLiteDatabase db = getWritableDatabase();
        String[] columns = {
                COLUMN_DADA_TIME,
                COLUMN_RESULT,
                COLUMN_DIAMETER,
                COLUMN_COUNT,
                COLUMN_NOTES};
        Cursor cursor = db.query(NAME_OF_TABLE, columns, null, null,
                null, null, null);
        if (cursor.moveToFirst()) {
            int dataTimeFromTable = cursor.getColumnIndex(COLUMN_DADA_TIME);
            int resultFromTable = cursor.getColumnIndex(COLUMN_RESULT);
            int diameterFromTable = cursor.getColumnIndex(COLUMN_DIAMETER);
            int countFromTable = cursor.getColumnIndex(COLUMN_COUNT);
            int notesFromTable = cursor.getColumnIndex(COLUMN_NOTES);
            do {
                DataModel data = new DataModel(
                        cursor.getString(dataTimeFromTable),
                        cursor.getString(resultFromTable),
                        cursor.getString(diameterFromTable),
                        cursor.getString(countFromTable),
                        cursor.getString(notesFromTable)
                );
                arrayList.add(data);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return arrayList;
    }

}
